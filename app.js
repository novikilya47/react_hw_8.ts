import express from 'express';
import userRouter from './routes/userRouter.js';
import mongoose from 'mongoose';
import cors from 'cors';

const app = express();

app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json())
app.use("/user",  userRouter);

mongoose.connect("mongodb://localhost:27017/", { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
    if (err) { return console.log('Something broke!'); }
    app.listen(3000, function () {
        console.log("Сервер работает");
    });
});