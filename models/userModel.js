import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
 
const userSchema = new mongoose.Schema({
    name: String, 
    email: {type: String, unique : true},
}, { versionKey: false });

userSchema.plugin(autopopulate);
const User = mongoose.model("user", userSchema); 

export { User, userSchema };