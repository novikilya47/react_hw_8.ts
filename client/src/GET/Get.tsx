import React, {useState} from 'react';
import {connect} from "react-redux";
import {getUsersList} from "../thunk/user.thunk";

function GetUser(props : any) {
    const [showBlock, setShowBlock] = useState(false)

    const userList = () => {
        props.getUsersList();
        setShowBlock(true)
    }
        return (
            <div className="Admin">
                <div className="State_buttons">
                        <button onClick={userList}>Получить список пользователей</button>
                </div>

                {props.user.loading ? <p>Loading</p> : <>{showBlock && props.user.array ? (<div>Список пользователей:</div>) : null}
                {showBlock && props.user.array ? props.user.array.map((array : any) => (
                    <ul key = {array._id}>
                        <li>
                            Пользователь {array.name};
                            Email: {array.email};
                        </li>
                    </ul>
                )) : null} </>}
            </div>
        )
}

const mapStateToProps = (state : any) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch : any) => ({
    getUsersList:() => {dispatch(getUsersList())},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetUser);