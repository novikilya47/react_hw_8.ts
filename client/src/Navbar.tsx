import React, {Component} from "react";
import { NavLink } from "react-router-dom";
import "./styles.css"

class Navbar extends Component{
    render(){
        return(
            <nav className="Navbar">
                <div>
                    <NavLink to = "/post">Add</NavLink>
                </div>
                <div>
                    <NavLink to = "/get">Get</NavLink>
                </div>
                <div>
                    <NavLink to = "/put">Put</NavLink>
                </div>
                <div>
                    <NavLink to = "/del">Del</NavLink>
                </div>
            </nav>
        )
    }
}

export default Navbar;