import {Provider} from 'react-redux';
import {store} from "./store/store";
import ADD from "./ADD/Add";
import GET from "./GET/Get";
import DEL from "./DEL/Del";
import PUT from "./PUT/Put"
import Navbar from "./Navbar";
import {BrowserRouter} from 'react-router-dom';
import {Route} from "react-router-dom";

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Route path='/post' component={ADD}/>
                <Route path='/get' component={GET}/>
                <Route path='/put' component={PUT}/>
                <Route path='/del' component={DEL}/>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
