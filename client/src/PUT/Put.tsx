import React, {useState} from 'react';
import {connect} from "react-redux";
import {changeUserName} from "../thunk/user.thunk";

function ChangeUser(props : any) {
    const [name, setName] = useState("Ilya");
    const [email, setEmail] = useState("dima@maill.ru");

    const newName = () => {
        let user = {
            name: name,
            email: email
        };

        props.changeUserName(user)

        setName('');
        setEmail('');
    }

        return (
            <div className = "Admin">
                <div className = "Admin_block">
                    <div>Форма изменения пользователя</div>
                    <div className = "Admin_block_field"><p>Почта пользователя: </p><textarea value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Новое имя пользователя: </p><textarea value={name} onChange={(e) => setName(e.target.value)}/></div>
                    <div className="State_buttons">
                        <button onClick={newName}>Отправить</button>
                    </div>  
                    <div>{props.user.message2}</div>                 
                </div>    
            </div>
        )
}

const mapStateToProps = (state : any) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch : any) => ({
    changeUserName: (data : any) => {dispatch(changeUserName(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangeUser);
