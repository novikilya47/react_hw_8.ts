import {ADD_USER, GET_USERS, PUT_USER, DELETE_USER} from "../Constants/constants";

export const addUser = (data: any) => ({type: ADD_USER, data});
export const getUsers = (data: any) => ({type: GET_USERS, data});
export const putUser= (data: any) => ({type: PUT_USER, data});
export const deleteUser = (data: any) => ({type: DELETE_USER, data});