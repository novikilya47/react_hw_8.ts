import { addUserToBack, getUsersToFront, delUserFromBack, putUserToBack } from "../services/user.service";
import { addUser, getUsers, deleteUser, putUser} from "../actionCreators/action";
import {GET_USERS_LOAD} from "../Constants/constants";

export const createUser = (data : any) => (dispatch : any) => {addUserToBack(data).then((data) => {dispatch(addUser(data))})};
export const getUsersList = () => (dispatch : any) => {
    dispatch({type: GET_USERS_LOAD})
    getUsersToFront()
        .then((data) => {
            setTimeout(() => {
                dispatch(getUsers(data));
            }, 1000);
        })
    };
export const delUser = (data : any) => (dispatch : any) => {delUserFromBack(data).then((data) => {dispatch(deleteUser(data))})};
export const changeUserName = (data : any) => (dispatch : any) => {putUserToBack(data).then((data) => {dispatch(putUser(data))})};
