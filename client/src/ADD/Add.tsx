import React, {useState} from 'react';
import {connect} from "react-redux";
import {createUser} from "../thunk/user.thunk";

interface IUser{
    message1: string
}

interface IProps{
    user: IUser,
    createUser: Function
}

function SendUser({user, createUser}: IProps) {
    const [name, setName] = useState("dima");
    const [email, setEmail] = useState("dima@maill.ru");

    const sendNewUser = () => {
        let user = {
            name: name,
            email: email
        };

        createUser(user)

        setName('');
        setEmail('');
    }

        return (
            <div className = "Admin">
                <div className = "Admin_block">
                    <div>Форма добавления пользователя</div>
                    <div className = "Admin_block_field"><p>Имя пользователя: </p><textarea value={name} onChange={(e) => setName(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Почта пользователя: </p><textarea value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className="State_buttons">
                        <button onClick={sendNewUser}>Отправить</button>
                    </div> 
                    <div>{user.message1}</div>                  
                </div>
            </div>
        )
}

const mapStateToProps = (state : any) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch : any) => ({
    createUser: (data : any) => {dispatch(createUser(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SendUser);
