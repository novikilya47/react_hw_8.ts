import {createStore, combineReducers, applyMiddleware} from "redux";
import userReducer from '../reducers/user.reducer';
import thunk from "redux-thunk";

export const rootReducer = combineReducers({
    user: userReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
