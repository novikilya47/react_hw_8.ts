import { User } from "../models/userModel.js";

export const createUser = async function (req, res) {
    try {
        let userCreate = await User.create({ name: req.body.name, email: req.body.email});
        res.json(`Пользователь c почтой ${req.body.email} создан`);
    } catch (e) {
        return res.json(`Пользователь c почтой ${req.body.email} уже существует`);
    }
};

export const getAllUsers = async function (req, res) {
    try {
        let usersFind = await User.find({});
        res.json({usersFind});
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const deleteUser = async function (req, res) {
    try {
        let userDelete = await User.deleteOne({ email: req.body.email });
        res.json(`Пользователь ${req.body.email} удален`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const updateUser = async function (req, res) {
    try {
        let userUpdate = await User.updateOne({email: req.body.email}, {name: req.body.name });
        res.json(`Имя пользователя c почтой ${req.body.email} обновлено`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};