import express from 'express';
const userRouter = express.Router();
import { createUser, deleteUser, updateUser, getAllUsers } from "../controllers/userController.js";

userRouter.post("/create", createUser);
userRouter.delete("/delete", deleteUser);
userRouter.put("/update", updateUser);
userRouter.get("/getAll", getAllUsers);

export default userRouter;